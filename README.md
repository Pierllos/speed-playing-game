# Speed Playing Game

Un jeu open-source basé sur mon système SBCPG et sur les RPG. Idéal pour des parties rapides et entre amis !

# Auteur
Pierre-Aimé IMBERT : pai.logicielslibres@gmail.com

# Licence du jeu

Système de jeu : SBCPG
https://pierllos.gitlab.io

* Utilisation non-commerciale uniquement.
* Mon nom doit figurer si le projet est partagé par vos soins
* Une autorisation commerciale possible UNIQUEMENT par un contrat écrit et 
signé en mon nom avec le client ou l'entreprise concernée.

# A propos des images

* Images provenant de IconFinder : https://www.iconfinder.com/iconsets/fantasy-and-role-play-game-adventure-quest
* Merci à Chanut is Industries ! (https://www.iconfinder.com/Chanut-is)
* Attribution 3.0 Unported (CC BY 3.0) 
